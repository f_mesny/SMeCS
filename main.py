import do_perGenomeAnalysis
import get_data

import argparse
import sys
import subprocess
import os
import pandas as pd
import multiprocessing
import numpy as np
import gc



def get_params(argv):
    parser = argparse.ArgumentParser(description='Pipeline')
    parser.add_argument('-dir', '--dir', help="directory", required=True)
    parser.add_argument('-n', '--n', help="available CPUs", required=False, default=1)
    parser.add_argument('-len', '--len', help="Length of sliding window", required=False, default=1000)
    a = parser.parse_args()
    return a

def terminal(cmd): # performs a command line
    #Dir=a.o
    p = subprocess.Popen(cmd, shell=True, stdout = subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    p.wait()


def getDispersion(l): # builds a string describing the min, max and iqr values in a list
    ar=np.array(l)
    mi=round(min(l),1)
    ma=round(max(l),1)
    quartile_1, quartile_3 = np.percentile(ar, [25, 75])
    iqr = quartile_3 - quartile_1
    iqr=round(iqr,1)
    st='min=%s max=%s iqr=%s' % (str(mi),str(ma),str(iqr))
    return st
    
    
def parseGenome(contigfasta): # parses genome from a contig.fa file
    with open(contigfasta,'r') as fasta:
        genome=''
        contig_seq=''
        ln=0
        for line in fasta.readlines():
            ln+=1
            if ln==1:
                pass
            elif line[0]!='>':
                contig_seq=contig_seq+line[:-1]
            else:
                if len(contig_seq)>=200:
                    genome=genome+contig_seq
                contig_seq=''
        if len(contig_seq)>=200:
            genome=genome+contig_seq
    return genome
    
    
def topGO(Dir, f): # performs all the GO enrichment analyses
        try:
            if len([fi for fi in os.listdir(Dir+'/'+f) if "regions" in fi])>0:
                results=Dir+'/'+f
                dir_data=Dir+'/'+f+'/data'
                regionsCSV=pd.read_csv(results+'/rarePatternInReg.csv',sep=',')
                motifs=list(set(regionsCSV['motif']))
                for m in motifs:
                    patterns=list(set(regionsCSV[regionsCSV['motif']==m]['pattern']))
                    for p in patterns:
                        lm=[]
                        subRegionsCSV=regionsCSV[(regionsCSV['motif']==m) & (regionsCSV['pattern']==p)]
                        for listOfGene in subRegionsCSV['in_region']:
                            for thing in listOfGene[1:-1].replace("'","").split(', '):
                                if thing not in lm:
                                    lm.append(thing)
                        RArray=str(lm).replace('[','c(').replace(']',')')
                        terminal('Rscript '+Dir+'/_scripts/topGoAnalysis.R %s %s "%s"' % (dir_data,results+'/'+m+'-'+p.replace('/','s')+'_',RArray))
        except:
            print('error topGO (rare patterns) for ')
                    
        
        try:
            results=Dir+'/'+f
            dir_data=Dir+'/'+f+'/data'
            regionsCSV=pd.read_csv(results+'/regions.csv',sep=',')
            A5 = np.percentile(list(regionsCSV['%mA']), 5)
            
            lm=[]
            for listOfGene in regionsCSV[regionsCSV['%mA']<=A5]['in_region']:
                for thing in listOfGene[1:-1].replace("'",'').split(', '):
                    if thing not in lm:
                        lm.append(thing)
            with open(results+'/genesInUnmethRegs.txt','w+') as genestxt:
                genestxt.write(' '.join(lm))
            RArray=str(lm).replace('[','c(').replace(']',')')
            terminal('Rscript '+Dir+'/_scripts/topGoAnalysis.R %s %s "%s"' % (dir_data,results+'/underMeth5_',RArray))

            lm=[]
            for listOfGene in regionsCSV[regionsCSV['%mA']==0]['in_region']:
                for thing in listOfGene[1:-1].replace("'",'').split(', '):
                    if thing not in lm:
                        lm.append(thing)
            with open(results+'/genesInUndermethRegs.txt','w+') as genestxt:
                genestxt.write(' '.join(lm))
            RArray=str(lm).replace('[','c(').replace(']',')')
            terminal('Rscript '+Dir+'/_scripts/topGoAnalysis.R %s %s "%s"' % (dir_data,results+'/noMeth_',RArray))               
            
            
            
        except:
            print('error topGO (undermeth regions) for ',f)

            
        try:
            results=Dir+'/'+f
            dir_data=Dir+'/'+f+'/data'
            regionsCSV=pd.read_csv(results+'/regions_per_motif.csv',sep=',')
            motifs=list(set(regionsCSV['motif']))
            for m in motifs:
                lm=[]
                for listOfGene in regionsCSV[(regionsCSV['motif']==m) & (regionsCSV['enriched_in_motif']=='yes')]['in_region']:
                    for thing in listOfGene[1:-1].replace("'",'').split(', '):
                        if thing not in lm:
                            lm.append(thing)
                RArray=str(lm).replace('[','c(').replace(']',')')
                terminal('Rscript '+Dir+'/_scripts/topGoAnalysis.R %s %s "%s"' % (dir_data,results+'/'+m+'_',RArray))
        except:
            print('error topGO (regions enriched in motifs) for ',f)

    
    
    
    
    
    
    
if __name__ == '__main__':
    a = get_params(sys.argv[1:])
    Dir=a.dir
    n=int(a.n)
    l=a.len
    reg=int(l)
    dir_scripts=os.path.dirname(os.path.realpath(__file__))
    files =[f for f in os.listdir(Dir) if f[0]!='.' and f[0]!='_' and f[0]!='-' and f[-2:]!='.o' and f[-2:]!='.e']
    
    ############################################# GET DATA ##############################################
    pool = multiprocessing.Pool(n)
    for f in files:
        if 'data' not in os.listdir(Dir+'/'+f):
            print("ERROR: data not in '", f,"' folder")
        else:
            dir_data=Dir+'/'+f+'/data'
            pfam2go=get_data.getPfamToGoMapping(dir_scripts+'/pfam2go/pfam2go.txt')
            if 'motifs.gff' in os.listdir(dir_data) and 'contigs.fa' in os.listdir(dir_data) and 'hmmTblOutput.tsv' not in os.listdir(dir_data):
                pool.apply_async(get_data.getGeneToPfamToGO, args=(Dir, dir_data, pfam2go))
                

    pool.close()
    pool.join()
    pfam2go=None
    gc.collect()

    ######################################### PERFORM ANALYSES ##########################################
    pool = multiprocessing.Pool(n)
    filt='yes'

    for f in files:
#        if a.rm=='yes':
#            terminal("rm "+Dir+"/"+f+"/diffMeth.*;rm "+Dir+"/"+f+"/*.csv")
        t=[]

        if 'motifs.txt' not in os.listdir(Dir +"/"+f) and len(os.listdir(Dir +"/"+f+'/data/'))>=6:
            gomapping=Dir+'/'+f+'/data/GOmapping.tsv'
            o=Dir +"/"+f+"/"
            fa=Dir + "/"+f+"/data/contigs.fa"
            gff=Dir + "/"+f+"/data/motifs.gff"
            anno=Dir + "/"+f+"/data/"
            anno+=[file for file in os.listdir(Dir + "/"+f+"/data/") if file[-4:]==".gff" and file!="motifs.gff"][0]
            sp=f.split("-")[0].replace('_',' ')
            if len(sp.split(' '))>2:
                sp=sp.split(' ')[0]+' '+sp.split(' ')[1]
            print('Starting analysis of '+f+'...')
            pool.apply_async(do_perGenomeAnalysis.pipeForOne, args=(o,reg,fa,gff,anno,gomapping,sp,filt,f))
    pool.close()
    pool.join()

    ####################################### GO ENRICHMENT ANALYSES ########################################
    
    pool = multiprocessing.Pool(n)
    for f in files:
        pool.apply_async(topGO, args=(Dir, f))
    pool.close()
    pool.join()    
        

    ##################################### PARSE RESULTS FROM ANALYSES #####################################
    
    terminal("rm -r "+Dir +"/_results")
    terminal("mkdir "+Dir +"/_results")
    outputs={}
    files =[f for f in os.listdir(Dir) if f[0]!='.' and f[0]!='_' and f[0]!='-' and f[-2:]!='.o' and f[-2:]!='.e']
    noMotif=open(Dir +"/_results/noMotifIn.txt",'w+')
    for f in files:
        if 'motifs.txt' not in os.listdir(Dir +"/"+f):
            noMotif.write(f+' pipe not performed\n')
        else:
            with open(Dir +"/"+f+"/motifs.txt",'r') as fi:
                string=fi.read()
            methylases=pd.read_csv(Dir +"/"+f+"/methylases.csv", sep=',')
            genome=parseGenome(Dir +"/"+f+"/data/contigs.fa")
            leng=len(genome)
            sumDf=pd.read_csv(Dir +"/"+f+"/summary.csv", sep=',')
                
            try:
                noMethCSV=pd.read_csv(Dir +"/"+f+"/noMeth_topGO_bpRes.csv", sep='\t')
            except:
                noMethCSV=pd.DataFrame()               
                
                
            try:
                underMethCSV5=pd.read_csv(Dir +"/"+f+"/underMeth5_topGO_bpRes.csv", sep='\t')
            except:
                underMethCSV5=pd.DataFrame()               
                
            outputs[f]={'motifs':string.split(' '),'methylasesDF':pd.read_csv(Dir +"/"+f+"/methylases.csv", sep=','),'summaryDF':sumDf,'genome_length':leng,'noMeth_regions':noMethCSV,'undermeth_regions5%':underMethCSV5,'margPatterns':{m:[ele for ele in list(sumDf[(sumDf['motif']==m) & (sumDf['percentage']<90) & (sumDf['count']>10)]['methylations']) if ele!=list(sumDf[sumDf['motif']==m]['methylations'])[0]] for m in string.split(' ')},'prophages':pd.read_csv(Dir +"/"+f+"/reg_prophages.csv",sep=','),'methylases':pd.read_csv(Dir +"/"+f+"/reg_methylases.csv",sep=','),'transposases':pd.read_csv(Dir +"/"+f+"/reg_transposases.csv",sep=',')}
    noMotif.close()
    
    
    ###################################### COMPARE AND SUM UP RESULTS ####################################
    
    terminal('mkdir '+Dir +"/_results/methylases_transposases_prophages")
    terminal('mkdir '+Dir +"/_results/GO_analysis_on_regs_enriched_in_motif")
    terminal('mkdir '+Dir +"/_results/GO_analysis_on_regs_containing_rare_patterns")
    terminal('mkdir '+Dir +"/_results/GO_analysis_on_undermethylated_regs")
    terminal('mkdir '+Dir +"/_results/motifs")
    terminal('mkdir '+Dir +"/_results/motifs_with_patterns")
    terminal('mkdir '+Dir +"/_results/motifs_in_species")
    
    allMotifs={}
    motifPerSpecies={}
    motifInSpecies={}
    dfGOumeth=pd.DataFrame()
    dfGOnometh=pd.DataFrame()
    dfProphages=pd.DataFrame()
    dfMethylases=pd.DataFrame()
    dfTransposases=pd.DataFrame()
    for sp in outputs:
        espece=sp.split("-")[0].replace('_',' ')
        if len(espece.split(' '))>2:
                espece=espece.split(' ')[0]+' '+espece.split(' ')[1]
        outputs[sp]['species']=espece
        if espece not in motifPerSpecies:
            motifPerSpecies[espece]=[]
            motifInSpecies[espece]={}
        motifPerSpecies[espece]=list(set(motifPerSpecies[espece]+outputs[sp]['motifs']))
        
        
        motifInSpecies[espece][sp]={motif:(motif in outputs[sp]['motifs']) for motif in motifPerSpecies[espece]}
        for motif in outputs[sp]['motifs']:
            if motif not in allMotifs:
                allMotifs[motif]=[]
            allMotifs[motif].append(sp)
        
        outputs[sp]['undermeth_regions5%']['species']=espece
        outputs[sp]['undermeth_regions5%']['genome']=sp
        dfGOumeth=dfGOumeth.append(outputs[sp]['undermeth_regions5%'])
        
        outputs[sp]['noMeth_regions']['species']=espece
        outputs[sp]['noMeth_regions']['genome']=sp
        dfGOnometh=dfGOnometh.append(outputs[sp]['noMeth_regions'])
        
        outputs[sp]['prophages']['species']=espece
        outputs[sp]['prophages']['genome']=sp
        dfProphages=dfProphages.append(outputs[sp]['prophages'])
        
        outputs[sp]['transposases']['species']=espece
        outputs[sp]['transposases']['genome']=sp
        dfTransposases=dfTransposases.append(outputs[sp]['transposases'])
        
        outputs[sp]['methylases']['species']=espece
        outputs[sp]['methylases']['genome']=sp
        dfMethylases=dfMethylases.append(outputs[sp]['methylases'])       
        
        
        outputs[sp]['methylases']=None
        outputs[sp]['transposases']=None
        outputs[sp]['prophages']=None
        gc.collect()

    dfMethylases.to_csv(Dir +"/_results/methylases_transposases_prophages/regionsWithMethylases.csv", sep=',', index=False)
    dfTransposases.to_csv(Dir +"/_results/methylases_transposases_prophages/regionsWithTransposases.csv", sep=',', index=False)
    dfProphages.to_csv(Dir +"/_results/methylases_transposases_prophages/regionsWithProphages.csv", sep=',', index=False)

    
    for espece in motifInSpecies:
        if len(motifInSpecies[espece])>1:
            dfpersp=pd.DataFrame(motifInSpecies[espece])
            dfpersp.to_csv(Dir +"/_results//motifs_in_species/"+espece+'.csv', sep=',')
            
            
    
    DIC={m:{} for m in allMotifs}
    dfOutput=[]
    patDF={}
    for motif in allMotifs:
        methDF=pd.DataFrame()
        sumDF=pd.DataFrame()
        goDF=pd.DataFrame()
        if len(allMotifs[motif])>1:
            patDF[motif]={}
            margPatterns=[]
            for sp in allMotifs[motif]:
                margPatterns+=outputs[sp]['margPatterns'][motif]
            margPatternsFiltered=[pat for pat in margPatterns if margPatterns.count(pat)>1]
            
            for sp in allMotifs[motif]:
                outputs[sp]['methylasesDF']['organism']=sp
                outputs[sp]['methylasesDF']['genome_length']=outputs[sp]['genome_length']
                outputs[sp]['methylasesDF']['1 motif every...']=outputs[sp]['methylasesDF']['genome_length']/outputs[sp]['methylasesDF']['Count']
                outputs[sp]['methylasesDF']['%In_CDS']=outputs[sp]['methylasesDF']['In_CDS']*100/outputs[sp]['methylasesDF']['Count']
                methDF=methDF.append(outputs[sp]['methylasesDF'][outputs[sp]['methylasesDF']['Motif']==motif])
                outputs[sp]['summaryDF']['organism']=sp
                sumDF=sumDF.append(outputs[sp]['summaryDF'][outputs[sp]['summaryDF']['motif']==motif])
                try:
                    goDFspMF=pd.read_csv(Dir+'/'+sp+'/'+motif+'_topGO_mfRes.csv',sep='\t')
                    goDFspMF['GOclass']='MF'
                    goDFspMF['organism']=sp
                    goDFspBP=pd.read_csv(Dir+'/'+sp+'/'+motif+'_topGO_bpRes.csv',sep='\t')
                    goDFspBP['GOclass']='BP'
                    goDFspBP['organism']=sp
                    goDF=goDF.append(goDFspMF)
                    goDF=goDF.append(goDFspBP)
                except:
                    pass
                
                for p in outputs[sp]['margPatterns'][motif]:
                    if p in margPatternsFiltered:
                        if p not in patDF[motif]:
                            patDF[motif][p]=pd.DataFrame()
                        try:
                            spMargPatBP=pd.read_csv(Dir+'/'+sp+'/'+motif+'-'+p.replace('/','s')+'_topGO_bpRes.csv',sep='\t')
                            spMargPatBP['organism']=sp
                            spMargPatBP['motif']=motif
                            spMargPatBP['pattern']=p
                            spMargPatBP['GOclass']='BP'
                            patDF[motif][p]=patDF[motif][p].append(spMargPatBP[spMargPatBP['classic']<0.05])
                        except:
                            print('[topGO] impossible to open',Dir+'/'+sp+'/'+motif+'-'+p.replace('/','s')+'_topGO_bpRes.csv')
                        try:
                            spMargPatMF=pd.read_csv(Dir+'/'+sp+'/'+motif+'-'+p.replace('/','s')+'_topGO_mfRes.csv',sep='\t')
                            spMargPatMF['organism']=sp
                            spMargPatMF['motif']=motif
                            spMargPatMF['pattern']=p
                            spMargPatMF['GOclass']='MF'
                            patDF[motif][p]=patDF[motif][p].append(spMargPatMF[spMargPatMF['classic']<0.05])
                        except:
                            print('[topGO] impossible to open',Dir+'/'+sp+'/'+motif+'-'+p.replace('/','s')+'_topGO_mfRes.csv')


            for p in patDF[motif]:
                patDF[motif][p]=patDF[motif][p].reindex(columns=["organism",'GOclass',"motif","pattern","GO.ID","Term","Annotated","Significant","Expected","classic", 'fdr'])
                patDF[motif][p].sort_values(by=['GOclass','organism','classic'],ascending=True)

                patDF[motif][p].to_csv(Dir +"/_results/GO_analysis_on_regs_containing_rare_patterns/"+motif+'.'+p.replace('/','s')+'.csv', sep=',', index=False)

            
            methDF.to_csv(Dir +"/_results/motifs/"+motif+'.csv', sep=',', index=False)
            sumDF.to_csv(Dir +"/_results/motifs_with_patterns/"+motif+'.csv', sep=',', index=False)
            goDF=goDF.reindex(columns=["GOclass","organism","GO.ID","Term","Annotated","Significant","Expected","classic",'fdr'])
            goDF.sort_values(by=['GOclass','organism','classic'],ascending=True)
            goDF.to_csv(Dir +"/_results/GO_analysis_on_regs_enriched_in_motif/"+motif+'.csv', sep=',', index=False)
            
            

            for sp in allMotifs[motif]:
                if len(sumDF[sumDF['organism']==sp])>0:
                    pattern=sumDF[sumDF['organism']==sp].iloc[0]['methylations']
                    if pattern not in DIC[motif]:
                        DIC[motif][pattern]={}
                    DIC[motif][pattern][sp]=(sumDF[sumDF['organism']==sp].iloc[0]['count'],sumDF[sumDF['organism']==sp]['count'].sum())

            if len(DIC[motif])==0:
                pass
            else:
                if len(DIC[motif])==1:
                    pass
                    #print('no differential methylation of motif', motif)
                else:
                    pass
                    #print(' !!!!! differential methylation of motif', motif)
            for sign in DIC[motif]:
                disp=getDispersion([DIC[motif][sign][sp][0]*100/DIC[motif][sign][sp][1] for sp in DIC[motif][sign]])
                dfOutput.append({'Motif':motif,'MotifInSpecies':len(allMotifs[motif]),'Major_patterns':len(DIC[motif]),'Pattern':sign,'PatternInSpecies':len(DIC[motif][sign]),'PercentagesDispersion':disp,'Organisms':' '.join(DIC[motif][sign].keys())})
        else:
            sumDF=outputs[allMotifs[motif][0]]['summaryDF'][outputs[allMotifs[motif][0]]['summaryDF']['motif']==motif]
            try:
                pattern=sumDF.iloc[0]['methylations']
            except:
                methylations=[]
            dfOutput.append({'Motif':motif,'MotifInSpecies':1,'Major_patterns':1,'Pattern':pattern,'PatternInSpecies':1,'PercentagesDispersion':'','Organisms':' '.join(allMotifs[motif])})
    dfOutput=pd.DataFrame(dfOutput)
    try:
        dfOutput=dfOutput.sort_values(by=['MotifInSpecies','Motif','PatternInSpecies'],ascending=False)
        dfOutput=dfOutput.reindex(columns=['Motif','MotifInSpecies','Major_patterns','Pattern','PatternInSpecies','PercentagesDispersion','Organisms'])
        dfOutput.to_csv(Dir +"/_results/general_report.csv", sep=',', index=False)
    except:
        print('cannot export general_report.csv')
        

        
    
    sumGODf=[]
    dfGOnometh['classic']=dfGOnometh['classic'].replace('< 1e-30', 1e-30)
    dfGOnometh['classic']=dfGOnometh['classic'].astype(float)

    strings=[cl for cl in dfGOnometh['classic'] if type(cl) is str]

    for go in list(set(dfGOnometh[dfGOnometh['classic']<0.05]['GO.ID'])):
        try:
            sumGODf.append({'GO.ID':go,'Term':list(dfGOnometh[dfGOnometh['GO.ID']==go]['Term'])[0],'in # genomes':len(dfGOnometh[(dfGOnometh['classic']<0.05) & (dfGOnometh['GO.ID']==go)]),'in # species': len(set(dfGOnometh[(dfGOnometh['classic']<0.05) & (dfGOnometh['GO.ID']==go)]['species'])),'mean_p':dfGOnometh[(dfGOnometh['classic']<0.05) & (dfGOnometh['GO.ID']==go)]['classic'].sum()/len(dfGOnometh[(dfGOnometh['classic']<0.05) & (dfGOnometh['GO.ID']==go)]), 'genomes':list(set(dfGOnometh[(dfGOnometh['classic']<0.05) & (dfGOnometh['GO.ID']==go)]['genome'])),'species':list(set(dfGOnometh[(dfGOnometh['classic']<0.05) & (dfGOnometh['GO.ID']==go)]['species']))})
        except:
            print('error here1')

 
    sumGODF=pd.DataFrame(sumGODf)
    try:
        sumGODF=sumGODF.sort_values(by=['in # species','mean_p'],ascending=True)
        sumGODF=sumGODF.reindex(columns=['GO.ID','Term','in # genomes','in # species','mean_p','species','genomes'])
        sumGODF.to_csv(Dir +"/_results/GO_analysis_on_undermethylated_regs/unmethylatedRegions.csv", sep=',', index=False)
    except:
        print('cannot export sumGODf.csv')    


    sumGODf=[]
    dfGOumeth['classic']=dfGOumeth['classic'].replace('< 1e-30', 1e-30)
    dfGOumeth['classic']=dfGOumeth['classic'].astype(float)
    for go in list(set(dfGOumeth[dfGOumeth['classic']<0.05]['GO.ID'])):
        try:
            sumGODf.append({'GO.ID':go,'Term':list(dfGOumeth[dfGOumeth['GO.ID']==go]['Term'])[0],'in # genomes':len(dfGOumeth[(dfGOumeth['classic']<0.05) & (dfGOumeth['GO.ID']==go)]),'in # species': len(set(dfGOumeth[(dfGOumeth['classic']<0.05) & (dfGOumeth['GO.ID']==go)]['species'])),'mean_p':dfGOumeth[(dfGOumeth['classic']<0.05) & (dfGOumeth['GO.ID']==go)]['classic'].sum()/len(dfGOumeth[(dfGOumeth['classic']<0.05) & (dfGOumeth['GO.ID']==go)]), 'genomes':list(set(dfGOumeth[(dfGOumeth['classic']<0.05) & (dfGOumeth['GO.ID']==go)]['genome'])),'species':list(set(dfGOumeth[(dfGOumeth['classic']<0.05) & (dfGOumeth['GO.ID']==go)]['species']))})
        except:
            print('error here5')    
   
    sumGODF=pd.DataFrame(sumGODf)
    try:
        sumGODF=sumGODF.sort_values(by=['in # species','mean_p'],ascending=True)
        sumGODF=sumGODF.reindex(columns=['GO.ID','Term','in # genomes','in # species','mean_p','species','genomes'])
        sumGODF.to_csv(Dir +"/_results/GO_analysis_on_undermethylated_regs/undermethylatedRegions.csv", sep=',', index=False)
    except:
        print('cannot export sumGODf.csv')    


