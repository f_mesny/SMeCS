import sys
import subprocess
import os
import pandas as pd
import numpy as np
import re
cwd = os.getcwd()
#print(cwd)


def complementary(seq):
    rseq=seq.replace('A','t').replace('T','a').replace('G','c').replace('C','g').replace('R','y').replace('Y','r').replace('S','w').replace('W','s').replace('M','k').replace('K','m').replace('B','v').replace('V','b').replace('D','h').replace('H','d')
    rseq=rseq.upper()
    return rseq

def returnSeqs(string):
        iupac={'R':['A','G'],'Y':['C','T'],'S':['G','C'],'W':['A','T'],'K':['G','T'],'M':['A','C'],'B':['C','G','T'],'D':['A','G','T'],'H':['C','A','T'],'V':['C','A','G'],'N':['C','A','G','T']}
        l=[]
        #print(string)
        for n in range(len(string)):
                if string[n] in iupac.keys():
                        if len(l)==0:
                                for i in iupac[string[n]]:
                                        l.append(string[:n]+i)
                        else:
                                for j in range(len(l)):
                                        for i in iupac[string[n]]:
                                                l.append(l[j]+i)
                elif len(l)>0:
                         for j in range(len(l)):
                                l[j]=l[j]+string[n]
        l=[ele for ele in l if len(ele)==len(string)]
        if len(l)==0:
            l.append(string)
        return l

def signatureSyntax(motifstring, listofmethylatedpositions, strand):
    lower=motifstring.lower()
    llower=list(lower)
    if strand=='r' or strand=='c':
        llower=llower[::-1]
        for pos in listofmethylatedpositions:
            llower[pos]=llower[pos].upper()
        llower=llower[::-1]
    else:
        for pos in listofmethylatedpositions:
            llower[pos]=llower[pos].upper()
    signature=''.join(llower)
    return signature

def dsSignature(motifstring, listofmethylatedpositions1, listofmethylatedpositions2, strand1):
    str1=signatureSyntax(motifstring, listofmethylatedpositions1, strand1)
    if strand1=='s':
        str2=signatureSyntax(complementary(motifstring), listofmethylatedpositions2, 's')
    else:
        str2=signatureSyntax(complementary(motifstring), listofmethylatedpositions2, 'r')
    signature=str1+'/'+str2
    return signature

def isPalindromic(motif):
    cmotif=complementary(motif)
    return motif[::-1]==cmotif


def getPattern(sign):
    if isPalindromic(sign['motif']) and len(sign['- meth'])!=len(sign['+ meth']):
        if len(sign['- meth'])>len(sign['+ meth']):
            pattern=str(str(sign['- meth'])+'/'+str(sign['+ meth'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            ipds=str(str(sign['- IPDs'])+'/'+str(sign['+ IPDs'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            qvs=str(str(sign['-QV'])+'/'+str(sign['+QV'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
        elif len(sign['- meth'])<len(sign['+ meth']):
            pattern=str(str(sign['+ meth'])+'/'+str(sign['- meth'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            ipds=str(str(sign['+ IPDs'])+'/'+str(sign['- IPDs'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            qvs=str(str(sign['+QV'])+'/'+str(sign['-QV'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
    elif isPalindromic(sign['motif']) and len(sign['- meth'])==len(sign['+ meth']):
        st1=[int(letter) for letter in str(sign['+ meth']).replace(' ','').replace('[','').replace(']','') if letter!=',']
        st2=[int(letter) for letter in str(sign['- meth']).replace(' ','').replace('[','').replace(']','') if letter!=',']    
        if sum(st1)>=sum(st2):
            pattern=str(str(sign['+ meth'])+'/'+str(sign['- meth'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            ipds=str(str(sign['+ IPDs'])+'/'+str(sign['- IPDs'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            qvs=str(str(sign['+QV'])+'/'+str(sign['-QV'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
        else:
            pattern=str(str(sign['- meth'])+'/'+str(sign['+ meth'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            ipds=str(str(sign['- IPDs'])+'/'+str(sign['+ IPDs'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            qvs=str(str(sign['-QV'])+'/'+str(sign['+QV'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
    elif not isPalindromic(sign['motif']):
        if sign['strand']=='+':
            pattern=str(str(sign['+ meth'])+'/'+str(sign['- meth'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            ipds=str(str(sign['+ IPDs'])+'/'+str(sign['- IPDs'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            qvs=str(str(sign['+QV'])+'/'+str(sign['-QV'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
        elif sign['strand']=='-':
            pattern=str(str(sign['- meth'])+'/'+str(sign['+ meth'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            ipds=str(str(sign['- IPDs'])+'/'+str(sign['+ IPDs'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
            qvs=str(str(sign['-QV'])+'/'+str(sign['+QV'])).replace(' ','').replace('[','').replace(']','').replace(',','-')
    return (pattern,ipds,qvs)
        
        

def motifScore(motifstring):
    iupacforscore={'A':1,'T':1,'C':1,'G':1,'R':2,'Y':2,'S':2,'W':2,'K':2,'M':2,'B':3,'D':3,'H':3,'V':3,'N':4,'X':1}
    a=1
    for letter in motifstring:
        a=a*iupacforscore[letter]
    score=len(motifstring)-(a/len(motifstring))
    return score


def bestInTuple(tupl): #retrieves the less-stringent motif in a tuple
    if tupl[0] in tupl[1]:
        return tupl[0]
    elif tupl[1] in tupl[0]:
        return tupl[1]
    else:
        seqs0=returnSeqs(tupl[0])
        seqs1=returnSeqs(tupl[1])
        for i in seqs0:
            for j in seqs1:
                if 'X' in j and 'X' in i:
                    jp1=j.split('X')[0]
                    jp2=j.split('X')[-1]
                    ip1=i.split('X')[0]
                    ip2=i.split('X')[-1]
                    if len(i)==len(j):
                        if motifScore(ip1)<=motifScore(jp1):
                            m=ip1
                        else:
                            m=jp1
                        if motifScore(ip2)<=motifScore(jp2):
                            xreq=len(i)-len(m)-len(ip2)
                            m=m+''.join(['X' for x in range(xreq)])+ip2
                        else:
                            xreq=len(i)-len(m)-len(jp2)
                            m=m+''.join(['X' for x in range(xreq)])+jp2
                        return m
                    else:
                        if motifScore(i)<= motifScore(j):
                            return i
                        else:
                            return j
                elif i.find(j)!=-1 and motifScore(tupl[0][i.find(j):i.find(j)+len(j)])<min(motifScore(tupl[0]),motifScore(tupl[1])):
                    return tupl[0][i.find(j):i.find(j)+len(j)]
                elif j.find(i)!=-1 and motifScore(tupl[1][j.find(i):j.find(i)+len(i)])<min(motifScore(tupl[0]),motifScore(tupl[1])):
                    return tupl[1][j.find(i):j.find(i)+len(i)]
                
                elif len(tupl[0])==len(tupl[1]):
                    string=''
                    for i in range(min(len(tupl[1]),len(tupl[0]))):
                        if motifScore(tupl[0][i])<=motifScore(tupl[1][i]):
                            string+=tupl[0][i]
                        else:
                            string+=tupl[1][i]
                    return string
                elif len(j)>len(i) and j.find(i)!=-1:
                    seq1=tupl[1][j.find(i):j.find(i)+len(i)]
                    string=''
                    for w in range(len(seq1)):
                        if motifScore(seq1[w])<=motifScore(tupl[0][w]):
                            string+=seq1[w]
                        else:
                            string+=tupl[0][w]
                    return string
                elif len(i)>len(j) and i.find(j)!=-1:
                    seq0=tupl[0][i.find(j):i.find(j)+len(j)]
                    string=''
                    for x in range(len(seq0)):
                        if motifScore(seq0[x])<=motifScore(tupl[1][x]):
                            string+=seq0[x]
                        else:
                            string+=tupl[1][x]
                    return string
                
def allCorresp(l): #returns a dictionnary {potential_sequence:[motif1]}
    decoding={}
    for seq in l:
        for subseq in returnSeqs(seq):
            if subseq not in decoding:
                decoding[subseq]=[]
            decoding[subseq].append(seq)
    return decoding

def filterMotifList(l): #filters list of motifs to remove redundancy
    decoding=allCorresp(l)
    unique=[]
    filteredl=[]
    l2=[]
    for ele in decoding:
        if 'X' in ele:
            for t in decoding[ele]:
                stend=(t.split('X')[0],t.split('X')[-1])
                sim=list(set([(t, decoding[autrele][0]) for autrele in decoding if ele!=autrele and t!=decoding[autrele][0] and 'X' in autrele and decoding[autrele][0].split('X')[0] in stend[0] and decoding[autrele][0].split('X')[-1] in stend[1]]))
                if len(sim)>0:
                    l2+=sim
                else:
                    unique.append(decoding[ele][0])
        elif len(decoding[ele])==1:
            sim=[(decoding[ele][0], decoding[autrele][0]) for autrele in decoding if ele!=autrele and autrele in ele]
            if len(sim)>0:
                l2+=sim
            else:
                unique.append(decoding[ele][0])
        else:
            l2+=[(decoding[ele][1], decoding[autrele][0]) for autrele in decoding if autrele in ele]
        l2=list(set(l2))
        unique=list(set(unique))

    for tupl in l2:
        if tupl[0] in unique:
            unique.remove(tupl[0])
        if tupl[1] in unique:
            unique.remove(tupl[1])
        bst=bestInTuple(tupl)
        filteredl.append(bst)
    filteredl+=unique
    return filteredl



    
def getMotifs(l): #runs filterMotifList as much as needed before and after rebase motifs have been introduced
    for ele in set(l): #before to filter by comparison, we cut the low-quality ends of some motifs (when they decrease the motif score)
        s=ele
        if 'X' in s:
            l.append(ele)
        elif motifScore(ele)<4:
            while motifScore(s[1:])>motifScore(s) or s[0]=='N':
                s=s[1:]
            while motifScore(s[:-1])>motifScore(s) or s[-1]=='N':
                s=s[:-1]
            l.remove(ele)
            l.append(s)
    while len(filterMotifList(l))<len(l):
        l=filterMotifList(l)

    while len(filterMotifList(l))<len(l):
        l=filterMotifList(l)
    return l


def appendToPosDic(CDSpos, start, end, name, qualifier):
    if start in CDSpos and start+1 in CDSpos:
        pass
        #print('ERROR: start and start+1 in CDSpos',CDSpos[start],CDSpos[start+1],name)
    elif start in CDSpos:
        CDSpos[start+1]=('start',name,qualifier)
    else:
        CDSpos[start]=('start',name,qualifier) #{pos:(start,ID)}
    if end in CDSpos and end-1 in CDSpos:
        pass
        #print('ERROR: end and end-1 in CDSpos',CDSpos[end],CDSpos[end-1],name)
    elif end in CDSpos:
        CDSpos[end-1]=CDSpos[end]
        CDSpos[end]=('end',name,qualifier)
    else:
        CDSpos[end]=('end',name,qualifier) #{pos:(start,ID)}
    return CDSpos

def genomicContext(CDSpos, keys, myNumber):
    #keys=sorted(CDSpos.keys())
    closestkey=min(keys, key=lambda x:abs(myNumber-x))
    ind=keys.index(closestkey)
    if closestkey>myNumber:
        prev=keys[ind-1]
        prevprev=keys[ind-2]
        nex=keys[ind]
        if CDSpos[prev][0]=='start' and CDSpos[nex][0]=='end':
            string='In '+CDSpos[prev][1]+' ('+CDSpos[prev][2]+')'
        elif CDSpos[prev][0]=='end' and CDSpos[nex][0]=='start':
            if CDSpos[prevprev][0]=='start' and CDSpos[prevprev][1]!=CDSpos[prev][1]:
                string='In '+CDSpos[prevprev][1]+' ('+CDSpos[prevprev][2]+')'
            elif CDSpos[prev][2]=='domain':
                string='Not in domain'
            else:
                string='In intergenic region'
        elif CDSpos[prev][0]=='end' and CDSpos[nex][0]=='end':
            string='In '+CDSpos[nex][1]+' ('+CDSpos[nex][2]+')'
        elif CDSpos[prev][0]=='start' and CDSpos[nex][0]=='start':
            string='In '+CDSpos[prev][1]+' ('+CDSpos[prev][2]+')'
        else:
            string='??'
    elif closestkey <= myNumber and len(keys)>ind+1:
        prev=keys[ind]
        prevprev=keys[ind-1]
        nex=keys[ind+1]
        if CDSpos[prev][0]=='start' and CDSpos[nex][0]=='end':
            string='In '+CDSpos[prev][1]+' ('+CDSpos[prev][2]+')'
        elif CDSpos[prev][0]=='end' and CDSpos[nex][0]=='start':
            if CDSpos[prevprev][0]=='start' and CDSpos[prevprev][1]!=CDSpos[prev][1]:
                string='In '+CDSpos[prevprev][1]+' ('+CDSpos[prevprev][2]+')'
            else:
                string='In intergenic region'
        elif CDSpos[prev][0]=='end' and CDSpos[nex][0]=='end':
            string='In '+CDSpos[nex][1]+' ('+CDSpos[nex][2]+')'
        elif CDSpos[prev][0]=='start' and CDSpos[nex][0]=='start':
            string='In '+CDSpos[prev][1]+' ('+CDSpos[prev][2]+')'
        else:
            string='?'
    elif closestkey <= myNumber:
        idx=keys[ind]
        if CDSpos[idx][0]=='start':
            string='In '+CDSpos[idx][1]+' ('+CDSpos[idx][2]+')'
        elif CDSpos[idx][0]=='end' and idx+1 in CDSpos and CDSpos[idx+1][0]=='start':
            string='In '+CDSpos[idx+1][1]+' ('+CDSpos[idx+1][2]+')'
        elif CDSpos[idx][0]=='end':
            string='In intergenic region'
    else:
        string=''
    return string


def outlierThresh(l):
    ys=np.array(l)
    quartile_1, quartile_3 = np.percentile(ys, [25, 75])
    iqr = quartile_3 - quartile_1
    #lower_bound = quartile_1 - (iqr * 1.5)
    upper_bound = quartile_3 + (iqr * 1.5)
    return upper_bound



def filterDoubleMotifs(l):
    motifsnoN=[]
    thresh='NNN'
    for motif in l:
        if thresh in motif:
            Nstart=motif.find(thresh)
            i=0
            n=thresh
            while motif[Nstart+len(thresh)+i]=='N':
                n+='N'
                i+=1
            p1p2=motif.split(n)
            x=n.replace('N','X')
            newmotif=x.join(p1p2)
            motifsnoN.append(newmotif)
        else:
            motifsnoN.append(motif)    
    return motifsnoN


def scanGenome(genome,cgenome,motifs):
    r={} # [pos1,pos2,...,posn]:motif    for strand +
    s={} # for strand -
    for i in motifs:
        if 'X' in i:
            p1=i.split('X')[0]
            p2=i.split('X')[-1]
            for seq in returnSeqs(p1):
                for m in re.finditer(seq, genome):
                    startp2=m.start()+len(p1)+i.split('X').count('')+1
                    endp2=m.start()+len(i)
                    if genome[startp2:endp2] in returnSeqs(p2):
                        s[m.start()]={'pos':list(range(m.start()+1,endp2+1)),'motif':i,'seq':genome[m.start():endp2]}
            for seq in returnSeqs(p2):
                for m in re.finditer(seq[::-1], cgenome):
                    startp1=m.start()+len(p2)+i.split('X').count('')+1
                    endp1=m.start()+len(i)
                    if cgenome[startp1:endp1] in returnSeqs(p1[::-1]):
                        r[m.start()]={'pos':list(range(m.start(),endp1)),'motif':i,'seq':cgenome[m.start():endp1]}
        else:
            for seq in returnSeqs(i):
                for m in re.finditer(seq, genome):
                    s[m.start()]={'pos':list(range(m.start()+1,m.start()+1+len(i))),'motif':i,'seq':seq}
                for m in re.finditer(seq[::-1], cgenome):
                    r[m.start()]={'pos':list(range(m.start()+1,m.start()+1+len(i))),'motif':i,'seq':seq}
    return {'r':r,'s':s}


def pipeForOne(outp,reg,fa,gff,anno,gomapping,sp,filt,ref):
    ################################################# INITIALISATION #################################################
    if 'y' in filt:
        filtering=True
    else:
        filtering=False
    lenreg=int(reg)
    lenregmotif=lenreg
    clusters=['J', 'A', 'K', 'L', 'B', 'D', 'Y', 'V', 'T', 'M', 'N', 'Z', 'W', 'U', 'O', 'C', 'G', 'E', 'F', 'H', 'I', 'P', 'Q', 'R', 'S']
    iupac={'R':['A','G'],'Y':['C','T'],'S':['G','C'],'W':['A','T'],'K':['G','T'],'M':['A','C'],'B':['C','G','T'],'D':['A','G','T'],'H':['C','A','T'],'V':['C','A','G'],'N':['C','A','G','T']}
    ref='['+ref+']'
    ################################################# GENOME PARSING #################################################
    with open(fa,'r') as fasta:
        genome=''
        contigs={}
        contigNames={}
        contig_seq=''
        ln=0
        for line in fasta.readlines():
            ln+=1
            if ln==1:
                contignb=1
                contigs[contignb]=0
                contigNames[line[1:-1]]=contignb
            elif line[0]!='>':
                contig_seq=contig_seq+line[:-1]
            else:
                contigname_this=line[1:-1]
                if len(contig_seq)>=200:
                    contignb+=1
                    genome=genome+contig_seq
                    contigs[contignb]=len(genome)
                    contigNames[contigname_this]=contignb
                else:
                    pass
                contig_seq=''
        if len(contig_seq)>=200:
            genome=genome+contig_seq

        cgenome=complementary(genome)
    print(ref,'genome parsing OK')
    ############################################### motifs.gff PARSING ###############################################
    with open(gff, 'r') as in_handle:
        lines=in_handle.readlines()[3:]
    motifsPb=[]
    methylationIpdS={}
    methylationScoreS={}
    methylationIpdR={}
    methylationScoreR={}
    methylateds=[]
    methylatedr=[]

    with open(outp+'nomotif.gff','w+') as nomotif: #File with out-motif methylations
        for line in lines:
            if 'motif' not in line:
                nomotif.write(line)
            else:
                try:
                    motif=line.split(';motif=')[1].split(';')[0]
                    if motif not in motifsPb and ',' not in motif:
                        motifsPb.append(motif) #List of motifs contained in the genome
                except:
                    pass
            try:
                qual=line.split('\t')[6]
                cont=line.split('\t')[0]
                qv=line.split('\t')[5]
                pos=contigs[contigNames[cont]]+int(line.split('\t')[3])
                if '+' in qual:
                    methylationIpdS[pos]=float(line.split('IPDRatio=')[1].split(';')[0])
                    methylationScoreS[pos]=int(qv)
                    methylateds.append(pos)
                else:
                    methylationIpdR[pos]=float(line.split('IPDRatio=')[1].split(';')[0])
                    methylationScoreR[pos]=int(qv)
                    methylatedr.append(pos) #...and on strand -
            except:
                pass

    
    motifsPb=filterDoubleMotifs(motifsPb)
    print(ref,'motifs.gff parsing OK')
    
    ################################################# PARSE ANNOTATION ################################################
    
    with open(anno,'r') as an:
        lines=an.readlines()
        lines=lines[2:]
    CDSpos={}
    genetopos={}
    genetodesc={}
    genetoname={}
    for gene in lines:
        splitted=gene.split('\t')
        if len(splitted)>7:
            qualifiers={qual.split('=')[0]:qual.split('=')[1] for qual in splitted[8].split(';')}
            contignb=int(splitted[0][-6:])
            CDSpos=appendToPosDic(CDSpos, contigs[contignb]+int(splitted[3]), contigs[contignb]+int(splitted[4]), qualifiers['ID'], splitted[2])
            genetopos[qualifiers['ID']]=(contigs[contignb]+int(splitted[3]),contigs[contignb]+int(splitted[4]))
            genetodesc[qualifiers['ID']]=qualifiers['product']
            if gene in qualifiers:
                genetoname[qualifiers['ID']]=qualifiers['gene']
            else:
                genetoname[qualifiers['ID']]=''
    CDSkeys=sorted(CDSpos.keys())
    CDSkeysAr=np.array(CDSkeys)
    
    genetogo={}
    with open(gomapping, 'r') as gomap:
        lines=gomap.readlines()
    for line in lines:
        if '\t' in line and line.split('\t')[1]!='\n':
            gos=line[:-1].split('\t')[1].split(';')
        else:
            gos=[]
        genetogo[line.split('\n')[0].split('\t')[0]]=gos
    
    print(ref,'annotation data parsing OK')

    ################################################# GET REBASE DATA #################################################
    #try:
    #    rbdf=getRebase(sp,spnb,orgtable)
    #except:
    #    print('ERROR: Impossible to get data for species '+sp+' from Rebase')
    #    rbdf=pd.DataFrame([])
    #    motifs=getMotifs(motifs,[])
    
    motifs=getMotifs(motifsPb)
    
    print(ref,'motifs processing OK','\n\t_motifs found by PacBio:', motifsPb,'\n\t_motifs kept:',motifs)

    ############################################ SCAN GENOME FOR MOTIFS ###############################################
    scanned=scanGenome(genome,cgenome,motifs)
    r=scanned['r'] # [pos1,pos2,...,posn]:motif    for strand +
    s=scanned['s'] # for strand -
    print(ref,'genome scanning OK')
    
        
    ############################################## METHYLATIONS PER REGION ############################################    
    methperregionr={}
    methperregions={}
    regstartend={}
    regseq={}
    regseqc={}

    step=int(lenregmotif/10)

    for i in range(0,len(genome),step):
        if i+lenregmotif<=len(genome):
            regstartend[i]=i+lenregmotif
            regseq[i]=genome[i:i+lenregmotif]
            regseqc[i]=cgenome[i:i+lenregmotif]
        else:
            regstartend[i]=i+lenregmotif-len(genome)
            regseq[i]=genome[i:len(genome)]+genome[0:i+lenregmotif-len(genome)]
            regseqc[i]=cgenome[i:len(genome)]+genome[0:i+lenregmotif-len(genome)]
        methperregionr[i]=[pos for pos in methylatedr if pos>=i and pos<regstartend[i]]
        methperregions[i]=[pos for pos in methylateds if pos>=i and pos<regstartend[i]]
        

    regarray=np.array(list(methperregions.keys()))
    print(ref,"methylations per region OK")

    ###### CHECK IF THERE IS A METHYLATION IN EACH MOTIF, CREATES A GFF WITH METHYLATED AND UNMETHYLATED MOTIFS #######
    signatures=[]
    seqtomotif={}
    with open(outp+'.maum.gff','w+') as maum:#, open(outp+'.coords.csv') as coord:
        for ele in list(s.values()):
            methylatedposs={ele['pos'].index(i):methylationIpdS[i] for i in ele['pos'] if i in methperregions[regarray[regarray <= i].max()]}
            methylatedposr={ele['pos'].index(i):methylationIpdR[i] for i in ele['pos'] if i in methperregionr[regarray[regarray <= i].max()]}
            methylatedqvs={ele['pos'].index(i):methylationScoreS[i] for i in ele['pos'] if i in methperregions[regarray[regarray <= i].max()]}
            methylatedqvr={ele['pos'].index(i):methylationScoreR[i] for i in ele['pos'] if i in methperregionr[regarray[regarray <= i].max()]}
            if filtering:
                poss=[pos+1 for pos in methylatedposs if (pos+1 not in methylatedposs or methylatedposs[pos+1]<0.5*methylatedposs[pos]) and (pos-1 not in methylatedposs or methylatedposs[pos-1]<0.5*methylatedposs[pos])]
                posr=[pos+1 for pos in methylatedposr if (pos+1 not in methylatedposr or methylatedposr[pos+1]<0.5*methylatedposr[pos]) and (pos-1 not in methylatedposr or methylatedposr[pos-1]<0.5*methylatedposr[pos])]
            else:
                poss=[pos+1 for pos in methylatedposs]
                posr=[pos+1 for pos in methylatedposr]

            if len(methylatedposs)<1 and len(methylatedposr)<1: # if not any methylation in motif
                sign=ele['seq'].lower()+'/'+complementary(ele['seq']).lower()
                maum.write(ref+'\tCountMotifs.py\tunmethylated_motif\t'+str(ele['pos'][0])+'\t'+str(ele['pos'][-1])+'\t0\t+\t.\tmotif='+ele['motif']+';colour=2\n')
                signatures.append({'motif':ele['motif'],'methylation':sign,'start':ele['pos'][0],'end':ele['pos'][-1],'strand':'+','+ meth':str(poss),'- meth':str(sorted([len(ele['motif'])-k+1 for k in posr])),'+ IPDs':'','- IPDs':'', '+QV':'','-QV':''})
            else: # if methylation in motif
                sign=dsSignature(ele['seq'], methylatedposs, methylatedposr, 's')
                signatures.append({'motif':ele['motif'],'methylation':sign,'start':ele['pos'][0],'end':ele['pos'][-1],'strand':'+','+ meth':str(poss),'- meth':str(sorted([len(ele['motif'])-k+1 for k in posr])),'+ IPDs':str([methylatedposs[pos-1] for pos in poss]),'- IPDs':str([methylatedposr[pos-1] for pos in posr]), '+QV':str([methylatedqvs[pos-1] for pos in poss]),'-QV':str([methylatedqvr[pos-1] for pos in posr])})
                maum.write('genome\tCountMotifs.py\tmethylated_motif\t'+str(ele['pos'][0])+'\t'+str(ele['pos'][-1])+'\t0\t+\t.\tmotif='+ele['motif']+';colour=3\n')
            seqtomotif[sign]=ele['motif']

        for ele in list(r.values()):
            methylatedposs={ele['pos'].index(i):methylationIpdS[i] for i in ele['pos'] if i in methperregions[regarray[regarray <= i].max()]}
            methylatedposr={ele['pos'].index(i):methylationIpdR[i] for i in ele['pos'] if i in methperregionr[regarray[regarray <= i].max()]}
            methylatedqvs={ele['pos'].index(i):methylationScoreS[i] for i in ele['pos'] if i in methperregions[regarray[regarray <= i].max()]}
            methylatedqvr={ele['pos'].index(i):methylationScoreR[i] for i in ele['pos'] if i in methperregionr[regarray[regarray <= i].max()]}
            if filtering:
                poss=[pos for pos in methylatedposs if (pos+1 not in methylatedposs or methylatedposs[pos+1]<0.5*methylatedposs[pos]) and (pos-1 not in methylatedposs or methylatedposs[pos-1]<0.5*methylatedposs[pos])]
                posr=[pos for pos in methylatedposr if (pos+1 not in methylatedposr or methylatedposr[pos+1]<0.5*methylatedposr[pos]) and (pos-1 not in methylatedposr or methylatedposr[pos-1]<0.5*methylatedposr[pos])]
            else:
                poss=[pos for pos in methylatedposs]
                posr=[pos for pos in methylatedposr]
            
            if not any([a['start']==ele['pos'][0] and a['end']==ele['pos'][-1] and a['strand']=='+' for a in signatures]):
                if len(methylatedposs)<1 and len(methylatedposr)<1: # if not any methylation in motif
                    sign=ele['seq'].lower()+'/'+complementary(ele['seq']).lower()
                    maum.write(ref+'\tCountMotifs.py\tunmethylated_motif\t'+str(ele['pos'][0])+'\t'+str(ele['pos'][-1])+'\t0\t-\t.\tmotif='+ele['motif']+';colour=2\n')
                    signatures.append({'motif':ele['motif'],'methylation':sign,'start':ele['pos'][0],'end':ele['pos'][-1],'strand':'-','+ meth':str(sorted([len(ele['motif'])-k for k in poss])),'- meth':str(sorted([len(ele['motif'])-k for k in posr])),'+ IPDs':'','- IPDs':'', '+QV':'','-QV':''})
                else: # if methylation in motif
                    sign=dsSignature(ele['seq'], methylatedposr, methylatedposs, 'r')
                    signatures.append({'motif':ele['motif'],'methylation':sign,'start':ele['pos'][0],'end':ele['pos'][-1],'strand':'-','+ meth':str(sorted([len(ele['motif'])-k for k in poss])),'- meth':str(sorted([len(ele['motif'])-k for k in posr])),'+ IPDs':str([methylatedposs[pos] for pos in poss]),'- IPDs':str([methylatedposr[pos] for pos in posr]), '+QV':str([methylatedqvs[pos] for pos in poss]),'-QV':str([methylatedqvr[pos] for pos in posr])})
                    maum.write('genome\tCountMotifs.py\tmethylated_motif\t'+str(ele['pos'][0])+'\t'+str(ele['pos'][-1])+'\t0\t-\t.\tmotif='+ele['motif']+';colour=3\n')
                seqtomotif[sign]=ele['motif']
    countMotifsAndPatterns={}
    for motif in motifs:
        if motif not in countMotifsAndPatterns:
            countMotifsAndPatterns[motif]={}
    for sign in signatures:
        sign['context']=genomicContext(CDSpos, CDSkeys, sign['start'])
        pal=isPalindromic(sign['motif'])
        gene=sign['context'].split(" ")[1]
        pattern=getPattern(sign)
        sign['pattern']=pattern[0]
        sign['ipds']=pattern[1]
        sign['qvs']=pattern[2]
        if pattern[0] not in countMotifsAndPatterns[sign['motif']]:
            countMotifsAndPatterns[sign['motif']][pattern[0]]=0
        else:
            countMotifsAndPatterns[sign['motif']][pattern[0]]+=1
    print(ref,'methylation analysis for each motif OK')
    if len(signatures)==0:
        print(ref,'No motif found')
        print(ref,'DONE')
        return ''

    ############################# OCCURENCES #################################
    signDf=pd.DataFrame(signatures)
    signDf=signDf.sort_values(by=['start','strand'],ascending=True)
    signDf=signDf.reindex(columns=['start', 'end','strand','motif','methylation','pattern','ipds','qvs','context','domain_context'])
    signDf.to_csv(outp+'occurrences.csv', sep=',')
    print(ref,'occurences file OK')
    ############################# SUMMARY 2 #################################
    diktodf=[]


    for motif in countMotifsAndPatterns:
        for sign in countMotifsAndPatterns[motif]:
            subdf=signDf[signDf['motif']==motif]
            signSubDf=subdf[subdf['pattern']==sign]
            ### calculating mean IPD
            firstSt=pd.DataFrame([[float(i) for i in v.split('/')[0].split('-') if i!=''] for v in signSubDf['ipds']])
            firstStAv=[str(round(firstSt[i].sum()/len(firstSt[i]),1)) for i in firstSt.columns]
            secondSt=pd.DataFrame([[float(i) for i in v.split('/')[1].split('-') if i!=''] for v in signSubDf['ipds']])
            secondStAv=[str(round(secondSt[i].sum()/len(secondSt[i]),1)) for i in secondSt.columns]
            stringAvIPD='-'.join(firstStAv)+'/'+'-'.join(secondStAv)

            ## calculation mean QV
            firstSt=pd.DataFrame([[float(i) for i in v.split('/')[0].split('-') if i!=''] for v in signSubDf['qvs']])
            firstStAv=[str(round(firstSt[i].sum()/len(firstSt[i]),1)) for i in firstSt.columns]
            secondSt=pd.DataFrame([[float(i) for i in v.split('/')[1].split('-') if i!=''] for v in signSubDf['qvs']])
            secondStAv=[str(round(secondSt[i].sum()/len(secondSt[i]),1)) for i in secondSt.columns]
            stringAvQV='-'.join(firstStAv)+'/'+'-'.join(secondStAv)            

            diktodf.append({'motif':motif, 'methylations':str(sign),'mean_IPD':stringAvIPD,'mean_QV':stringAvQV,'count': len(signSubDf), 'In_CDS':len(signSubDf[signSubDf['context'].str.contains("CDS")]),'In_Intergenic':len(signSubDf[signSubDf['context'].str.contains("intergenic")]),'In_tRNA':len(signSubDf[signSubDf['context'].str.contains("tRNA")]),'In_rRNA':len(signSubDf[signSubDf['context'].str.contains("rRNA")]),'In_tmRNA':len(signSubDf[signSubDf['context'].str.contains("tmRNA")]),'percentage':len(signSubDf)*100/len(subdf)})
    sumDf=pd.DataFrame(diktodf)
    sumDf=sumDf.sort_values(by=['motif','count'],ascending=False)
    sumDf=sumDf.reindex(columns=['motif','methylations','mean_IPD','mean_QV','count','percentage','In_CDS','In_Intergenic','In_tRNA','In_rRNA','In_tmRNA'])
    
    marginalPatterns={}
    for m in motifs:
        majorPattern=list(sumDf[sumDf['motif']==m]['methylations'])[0]
        marginalPatterns[m]=[ele for ele in list(sumDf[(sumDf['motif']==m) & (sumDf['percentage']<90) & (sumDf['count']>10)]['methylations']) if ele!=majorPattern]
    

    sumDf.to_csv(outp+'summary.csv', sep=',',index=False)
    print(ref,'summary file OK')

    
    
    ############################# METHYLASES #################################
    thrp=0.05
    methSum=[]
    methsumlist=[]

    for m in motifs:
        subdfmotif=sumDf[sumDf['motif']==m]
        methSum.append({'Motif':m,'Count':subdfmotif['count'].sum(),'In_CDS':subdfmotif['In_CDS'].sum(),'In_Intergenic':subdfmotif['In_Intergenic'].sum(),'In_tRNA':subdfmotif['In_tRNA'].sum(),'In_rRNA':subdfmotif['In_rRNA'].sum(),'In_tmRNA':subdfmotif['In_tmRNA'].sum()})#'Enriched_in_genes':' '.join(enrichedingenes),'Enriched_in_domains':''+' '.join(enrichedindoms)})
        methSumDf=pd.DataFrame(methSum)
        methsumlist=list(methSumDf['Motif'])

    methSumDf2=pd.DataFrame(methSum)
    methSumDf2=methSumDf2.reindex(columns=['Enzyme','Motif','Count','In_CDS','In_Intergenic','In_tRNA','In_rRNA','In_tmRNA'])#,'Enriched_in_genes','Enriched_in_domains'])
    methSumDf2.to_csv(outp+'methylases.csv', sep=',',index=False)
    print(ref,'methylases file OK')
    
    ############################### MOTIFS AND METH PER REGION ############################
    methAperregion={}
    methCperregion={}
    realPatterns={}
    realValues={}
    for reg in regstartend:
        if reg<regstartend[reg]:
            subDfReg=signDf[(signDf['start']>=reg) & (signDf['start']<regstartend[reg])]
        else:
            subDfReg=signDf[(signDf['start']>=reg) | (signDf['start']<regstartend[reg])]
        
        ##### methylation per region ######
        countA=regseq[reg].count('A')
        if countA==0:
            countA=0.000000001
        countAc=regseqc[reg].count('A')
        if countAc==0:
            countAc=0.000000001
        countC=regseq[reg].count('C')
        if countC==0:
            countC=0.000000001
        countCc=regseqc[reg].count('C')
        if countCc==0:
            countCc=0.000000001        
        
        s1A=sum([1 for nt in methperregions[reg] if genome[nt-1]=='A'])/countA
        s2A=sum([1 for nt in methperregionr[reg] if cgenome[nt-1]=='A'])/countAc
        methAperregion[reg]=0.5*(s1A+s2A)

        realValues[reg]={}
        realPatterns[reg]={m:{} for m in subDfReg['motif']}
        for m in motifs:
            ###### regions enriched in motif m ######
            if m in realPatterns[reg]:
                realValues[reg][m]=len(subDfReg[subDfReg['motif']==m])
                for pattern in list(set(subDfReg[subDfReg['motif']==m]['pattern'])):
                    realPatterns[reg][m][pattern]=len(subDfReg[(subDfReg['motif']==m) & (subDfReg['pattern']==pattern)])

    regToVal={}
    for reg in regstartend:
        regToVal[reg]={m:{} for m in realPatterns[reg]}
        freq={'A':regseq[reg].count('A')/lenregmotif,'T':regseq[reg].count('T')/lenregmotif,'G':regseq[reg].count('G')/lenregmotif,'C':regseq[reg].count('C')/lenregmotif}
        for letter in iupac:
            freq[letter]=sum([freq[b] for b in iupac[letter]])

        for motif in motifs:
            theo=1
            for letter in motif:
                if letter!='X':
                    theo *= freq[letter]
            theo*=(lenregmotif-len(motif)+1)

            if motif in realValues[reg]:
                real=realValues[reg][motif]
            else:
                real=0
            if theo==0:
                theo=0.0000000001
            regToVal[reg][motif]={'exp':theo,'count':real,'ratio':real/theo, 'subs':real-theo}


    outlierThresholds={m:outlierThresh([regToVal[regi][motif]['ratio'] for regi in regToVal if motif in regToVal[regi] and regToVal[regi][motif]['ratio']>0]) for m in motifs}

    regDf=[]
    methylationsDf=[]
    margPatDf=[]
    
    thingsInRegion={}
    descInRegion={}
    for reg in regToVal:
        thingsInRegion[reg]=[ID for ID in genetopos if reg<genetopos[ID][0]<regstartend[reg] or reg<genetopos[ID][1]<regstartend[reg] or (regstartend[reg]>reg and genetopos[ID][0]<reg<regstartend[reg]<genetopos[ID][1]) or (regstartend[reg]<reg and (reg<genetopos[ID][0] or reg<genetopos[ID][1] or genetopos[ID][0]<regstartend[reg] or genetopos[ID][1]<regstartend[reg]))]
        descInRegion[reg]=[genetodesc[ID] for ID in thingsInRegion[reg] if ID in genetodesc]


    subsValuesForMotifs={m:[] for m in motifs}
    for reg in regToVal:
        deplMotif={}
        for motif in motifs:
            deplMotif[motif]=regToVal[reg][motif]['count']
            subsValuesForMotifs[motif].append(regToVal[reg][motif]['ratio'])
            if reg in realPatterns and motif in realPatterns[reg]:
                patterns=', '.join([str(realPatterns[reg][motif][p])+':'+p for p in realPatterns[reg][motif]])
            else:
                patterns=''
            if regToVal[reg][motif]['ratio'] > 0 and regToVal[reg][motif]['ratio']>outlierThresholds[motif]:
                outlier='yes'
            else:
                outlier='no'
                
            
            regDf.append({'motif':motif,'region_start':reg,'region_end':regstartend[reg],'in_region':str(thingsInRegion[reg]),'subs':regToVal[reg][motif]['subs'],'count':regToVal[reg][motif]['count'],'expected':regToVal[reg][motif]['exp'], 'descriptions':str(descInRegion[reg]),'patterns':patterns,'enriched_in_motif':outlier})
            if motif in realPatterns[reg]:
                for p in realPatterns[reg][motif]:
                    if p in marginalPatterns[motif]:
                        if motif in regToVal[reg] and regToVal[reg][motif]['count']==0:
                            Mcount=0.000001
                        elif motif in regToVal[reg]:
                            Mcount=regToVal[reg][motif]['count']
                        else:
                            Mcount=0.000001
                        margPatDf.append({'motif':motif,'pattern':p,'pattern_count':realPatterns[reg][motif][p],'pattern_pc':realPatterns[reg][motif][p]/Mcount,'motif_count':Mcount,'region_start':reg,'region_end':regstartend[reg],'in_region':str(thingsInRegion[reg]),'descriptions':str(descInRegion[reg])})
        motifs_in_region=[m for m in deplMotif if deplMotif[m]>0]
        try:
            methylationsDf.append({'region_start':reg,'region_end':regstartend[reg],'%mA':methAperregion[reg],'in_region':str(thingsInRegion[reg]), 'descriptions':str(descInRegion[reg]),'GOs':str([genetogo[gene] for gene in thingsInRegion[reg] if gene in genetogo]),'Nmotifs':sum(list(deplMotif.values())),'motifs':','.join(motifs_in_region),'patterns':' '.join([','.join(list(realPatterns[reg][motif].keys())) for motif in motifs_in_region if reg in realPatterns and motif in realPatterns[reg]])})
        except:
            print('error here')


    regDF=pd.DataFrame(regDf)
    regDF=regDF.sort_values(['motif','region_start'], ascending=False)
    regDF=regDF.reindex(columns=['motif','region_start','region_end','enriched_in_motif','subs','count','expected','patterns','in_region','descriptions'])
    regDF.to_csv(outp+'regions_per_motif.csv', sep=',',index=False)
    margPatDF=pd.DataFrame(margPatDf)
    margPatDF=margPatDF[margPatDF['pattern_count']>0]
    margPatDF=margPatDF.sort_values(['motif','pattern','region_start'], ascending=True)
    margPatDF=margPatDF.reindex(columns=['region_start','region_end','motif','motif_count','pattern','pattern_count','pattern_pc','in_region','descriptions'])
    margPatDF.to_csv(outp+'rarePatternInReg.csv', sep=',',index=False)    
    methylationsDF=pd.DataFrame(methylationsDf)
    methylationsDF.sort_values(by=['region_start'],ascending=True)
    methylationsDF=methylationsDF.reindex(columns=['region_start','region_end','%mA','in_region','descriptions','GOs','Nmotifs','motifs','patterns'])
    methylationsDF.to_csv(outp+'regions.csv', sep=',')

    
    ############### EXTRACT REGIONS WITH TRANSPOSASES, PROPHAGES, METHYLASES #######################
    transposasesDF=methylationsDF[methylationsDF['GOs'].str.contains('GO:0006313')]
    transposasesDF.to_csv(outp+'reg_transposases.csv', sep=',')
    virusDF=methylationsDF[((methylationsDF['descriptions'].str.contains('ntegrase')) & (methylationsDF['GOs'].str.contains('GO:0015074'))) | (methylationsDF['GOs'].str.contains('GO:0019058')) | (methylationsDF['GOs'].str.contains('GO:0016032'))]
    virusDF.to_csv(outp+'reg_prophages.csv', sep=',')
    methylasesDF=methylationsDF[methylationsDF['GOs'].str.contains('GO:0032775|GO:0006306')]
    methylasesDF.to_csv(outp+'reg_methylases.csv', sep=',')
    

    with open(outp+'motifs.txt','w+') as mfile:
        mfile.write(' '.join(motifs))
    print(ref,'DONE')
