import sys
import subprocess
import os

def terminal(cmd):
    p=subprocess.Popen(cmd, shell=True, stdout = subprocess.PIPE, stderr=subprocess.PIPE, stdin=subprocess.PIPE)
    p.wait()


def getData(Dir, outdir, folder):
    if folder not in os.listdir(outdir):
        terminal("mkdir "+outdir+"/"+folder)
        terminal("mkdir "+outdir+"/"+folder+"/data")
    elif 'data' not in os.listdir(outdir+"/"+folder):
        terminal("mkdir "+outdir+"/"+folder+"/data")
    terminal("cp "+Dir+"/motifs.gff "+outdir+"/"+folder+"/data/")
    terminal("cp "+Dir+"/contigs.fa "+outdir+"/"+folder+"/data/")
    terminal("cp "+Dir+"/annotation/*.faa "+outdir+"/"+folder+"/data/")
    terminal("cp "+Dir+"/annotation/*.gff "+outdir+"/"+folder+"/data/")
    print('OK for ',folder)

        
def getPfamToGoMapping(pfam2go):
    pfamtogo={}
    for line in open(pfam2go, "r").readlines():
        if line[0]!='!':
            if line.split(' ')[0][7:] not in pfamtogo:
                pfamtogo[line.split(' ')[0][5:]]=[]
            pfamtogo[line.split(' ')[0][5:]].append(line.split(' ')[-1][:-1])
    return pfamtogo
                
                
def getGeneToPfamToGO(Dir, dir_data, pfam2go):
    faa=dir_data+'/'+[file for file in os.listdir(dir_data) if file[-4:]==".faa"][0]
    terminal("hmmscan -o %s/hmmMainOutput --tblout %s/hmmTblOutput.tsv --domtblout %s/hmmDomOutput.tsv %s/_pfam2go/Pfam-A.hmm %s" % (dir_data, dir_data, dir_data, Dir, faa))
    with open(dir_data+'/hmmTblOutput.tsv', 'r') as tbl:
        lines=tbl.readlines()
    dic={}
    for line in lines:
        if line[0]!='#':
            lis=[ele for ele in line.split(' ') if ele!='']
            if lis[2] not in dic:
                dic[lis[2]]=[]
            dic[lis[2]].append(lis[1].split('.')[0])
            
    dic2={}
    with open(dir_data+'/GOmapping.tsv', 'w+') as out:
        for prot in dic:
            dic2[prot]=[]
            for ele in dic[prot]:
                if ele in pfam2go:
                    dic2[prot].append(';'.join(pfam2go[ele]))
            out.write(prot+'\t'+';'.join(dic2[prot])+'\n')
            
    
